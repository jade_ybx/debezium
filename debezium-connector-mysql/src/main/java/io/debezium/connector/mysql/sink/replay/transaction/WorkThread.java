/*
 * Copyright Debezium Authors.
 *
 * Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package io.debezium.connector.mysql.sink.replay.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.debezium.ThreadExceptionHandler;
import io.debezium.connector.breakpoint.BreakPointObject;
import io.debezium.connector.breakpoint.BreakPointRecord;
import io.debezium.connector.mysql.sink.object.ConnectionInfo;
import io.debezium.connector.mysql.sink.object.Transaction;
import io.debezium.connector.mysql.sink.util.SqlTools;
import io.debezium.enums.ErrorCode;

/**
 * Description: WorkThread class
 *
 * @author douxin
 * @since 2022-11-01
 **/
public class WorkThread extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkThread.class);
    private static final String BEGIN = "begin";
    private static final String COMMIT = "commit";
    private static final String ROLLBACK = "rollback";

    private final DateTimeFormatter sqlPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss.SSS");
    private ConnectionInfo connectionInfo;
    private int successCount;
    private int failCount;
    private Transaction txn = null;
    private final Object lock = new Object();
    private BlockingQueue<String> feedBackQueue;
    private List<String> failSqlList = new ArrayList<>();
    private BreakPointRecord breakPointRecord;
    private PriorityBlockingQueue<Long> replayedOffsets;
    private boolean isTransaction;
    private boolean isConnection = true;
    private boolean isAlive = true;

    /**
     * Constructor
     *
     * @param connectionInfo Connection the connection
     * @param feedBackQueue BlockingQueue<String> the feedBackQueue
     * @param index int the index
     * @param breakPointRecord record break point info
     */
    public WorkThread(ConnectionInfo connectionInfo, BlockingQueue<String> feedBackQueue,
                      int index, BreakPointRecord breakPointRecord) {
        super("work-thread-" + index);
        this.connectionInfo = connectionInfo;
        this.feedBackQueue = feedBackQueue;
        this.breakPointRecord = breakPointRecord;
        this.replayedOffsets = breakPointRecord.getReplayedOffset();
        this.isTransaction = true;
    }

    /**
     * Sets transaction
     *
     * @param transaction Transaction the transaction
     */
    public void setTransaction(Transaction transaction) {
        this.txn = transaction;
    }

    /**
     * Gets transaction
     *
     * @return Transaction the transaction
     */
    public Transaction getTransaction() {
        return this.txn;
    }

    /**
     * Clean transaction
     */
    public void cleanTransaction() {
        this.txn = null;
    }

    /**
     * Resume thread
     *
     * @param transaction Transaction the transaction
     */
    public void resumeThread(Transaction transaction) {
        synchronized (lock) {
            setTransaction(transaction);
            lock.notifyAll();
        }
    }

    /**
     * Pause thread
     */
    public void pauseThread() {
        synchronized (lock) {
            try {
                cleanTransaction();
                lock.wait();
            }
            catch (InterruptedException exp) {
                LOGGER.error("{}Interrupted exception occurred", ErrorCode.THREAD_INTERRUPTED_EXCEPTION, exp);
            }
        }
    }

    /**
     * Add fail transaction count
     */
    public void addFailTransaction() {
        failCount++;
    }

    @Override
    public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(new ThreadExceptionHandler());
        try (Connection connection = connectionInfo.createOpenGaussConnection();
                Statement statement = connection.createStatement()) {
            while (isConnection) {
                pauseThread();
                replayTransaction(statement, connection);
            }
        }
        catch (Throwable exp) {
            LOGGER.error("{}Exception occurred in work thread {} and the exp message is {}",
                    ErrorCode.DB_CONNECTION_EXCEPTION, this.getName(), exp.getMessage());
        }
    }

    private void replayTransaction(Statement statement, Connection connection) throws SQLException {
        boolean shouldStartTransaction = txn.getSqlList().size() > 1;
        if (shouldStartTransaction) {
            statement.execute(BEGIN);
        }
        boolean isSuccess = executeTxnSql(statement, connection);
        if (isSuccess) {
            if (shouldStartTransaction) {
                statement.execute(COMMIT);
            }
            successCount++;
        }
        else {
            if (shouldStartTransaction && isConnection) {
                statement.execute(ROLLBACK);
            }
            failCount++;
            List<String> tmpSqlList = new ArrayList<>();
            tmpSqlList.add("-- " + sqlPattern.format(LocalDateTime.now()) + ": " + txn.getSourceField());
            tmpSqlList.add("-- " + txn.getExpMessage());
            tmpSqlList.addAll(txn.getSqlList());
            tmpSqlList.add(System.lineSeparator());
            failSqlList.addAll(tmpSqlList);
        }
        if (isConnection) {
            buildAndSaveBpInfo();
        }
    }

    /**
     * Can the thread be available
     *
     * @return boolean the canUse
     */
    public boolean canUse() {
        return isAlive;
    }

    /**
     * Sets alive
     *
     * @param alive boolean the alive
     */
    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    private boolean executeTxnSql(Statement statement, Connection connection) {
        for (String sql : txn.getSqlList()) {
            try {
                statement.execute(sql);
            }
            catch (SQLException exp) {
                if (!connectionInfo.checkConnectionStatus(connection)) {
                    isConnection = false;
                    return false;
                }
                LOGGER.error("{}SQL exception occurred in transaction {}", ErrorCode.SQL_EXCEPTION,
                    txn.getSourceField());
                LOGGER.error("{}The error SQL statement executed is: {}", ErrorCode.SQL_EXCEPTION, sql);
                LOGGER.error("{}the cause of the exception is {}", ErrorCode.SQL_EXCEPTION, exp.getMessage());
                txn.setExpMessage(exp.getMessage());
                return false;
            }
            finally {
                feedBackModifiedTable();
            }
        }
        return true;
    }

    private void buildAndSaveBpInfo() {
        if (txn != null) {
            if (txn.getIsDml()) {
                replayedOffsets.add(txn.getTxnBeginOffset());
                replayedOffsets.addAll(txn.getSqlOffsets());
                replayedOffsets.add(txn.getTxnEndOffset());
            } else {
                replayedOffsets.add(txn.getTxnBeginOffset());
            }
            savedBreakPointInfo(txn);
        }
    }

    /**
     * get success count
     *
     * @return count of replayed successfully
     */
    public int getSuccessCount() {
        return this.successCount;
    }

    /**
     * get fail sql list
     *
     * @return List the fail sql list
     */
    public List<String> getFailSqlList() {
        return failSqlList;
    }

    /**
     * Save breakpoint data to kafka
     *
     * @param txn the replay transaction
     */
    private void savedBreakPointInfo(Transaction txn) {
        BreakPointObject txnBpObject = new BreakPointObject();
        txnBpObject.setBeginOffset(txn.getTxnBeginOffset());
        txnBpObject.setEndOffset(txn.getTxnEndOffset());
        txnBpObject.setTimeStamp(LocalDateTime.now().toString());
        if (!txn.getSourceField().getGtid().isEmpty()) {
            txnBpObject.setGtid(txn.getSourceField().getGtid());
        }
        breakPointRecord.storeRecord(txnBpObject, isTransaction);
    }

    /**
     * clear fail sql list
     */
    public void clearFailSqlList() {
        failSqlList.clear();
    }

    /**
     * get fail count
     *
     * @return int the fail count
     */
    public int getFailCount() {
        return failCount;
    }

    private void feedBackModifiedTable() {
        if (!txn.getIsDml() && SqlTools.isCreateOrAlterTableStatement(txn.getSqlList().get(1))) {
            String schemaName = txn.getSourceField().getDatabase();
            String tableName = txn.getSourceField().getTable();
            String tableFullName = schemaName + "." + tableName;
            feedBackQueue.add(tableFullName);
        }
    }
}
