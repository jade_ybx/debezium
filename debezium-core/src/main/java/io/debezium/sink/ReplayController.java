/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2025-2025. All rights reserved.
 */

package io.debezium.sink;

/**
 * sink replay controller
 *
 * @author jianghongbo
 * @since 2025/2/6
 */
public interface ReplayController {
}
